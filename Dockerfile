FROM ubuntu:20.04

ARG RUBY_ENV="development" \
    DEBIAN_FRONTEND=noninteractive

ENV RUBY_ENV=$RUBY_ENV \
    TZ=UTC \
    ASDF_DATA_DIR=/asdf \
    ASDF_DIR=/asdf
    
RUN groupadd -r deployer && \
    useradd -m -g deployer deployer

RUN sed -i 's/htt[p|ps]:\/\/archive.ubuntu.com\/ubuntu\//mirror:\/\/mirrors.ubuntu.com\/mirrors.txt/g' /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -y ca-certificates && \
    rm -rf /var/lib/apt/lists/*

RUN apt-get update && \
    apt-get install -y libv8-dev git gnupg curl libcurl4-openssl-dev libpq-dev shared-mime-info redis-server build-essential zlib1g zlib1g-dev libreadline-dev uuid-dev libsqlite3-dev libyaml-dev pdftk && \
    rm -rf /var/lib/apt/lists/*

# Install packages needed for the libreoffice
RUN apt-get update && \
        apt-get install -y wget libdbus-1-3 libcairo2 libcups2 libsm6 libxinerama1

# Install a specific libreoffice version
RUN wget https://downloadarchive.documentfoundation.org/libreoffice/old/6.4.7.2/deb/x86_64/LibreOffice_6.4.7.2_Linux_x86-64_deb.tar.gz && \
    tar xvzf LibreOffice_6.4.7.2_Linux_x86-64_deb.tar.gz && dpkg -i LibreOffice_6.4.7.2_Linux_x86-64_deb/DEBS/*.deb && rm LibreOffice_6.4.7.2_Linux_x86-64_deb.tar.gz && rm -rf LibreOffice_6.4.7.2_Linux_x86-64_deb && \
    ln -s $(which libreoffice6.4) /usr/bin/libreoffice && ln -s $(which libreoffice6.4) /usr/bin/soffice

RUN printf '#!/bin/sh\n . /asdf/asdf.sh && rails server -P /tmp/rails.pid' > /opt/entrypoint.sh && \
    chmod a+rwx /opt/entrypoint.sh

CMD /opt/entrypoint.sh

RUN mkdir /asdf

USER deployer

USER root

RUN chown -R deployer:deployer /asdf

USER deployer

RUN git clone https://github.com/asdf-vm/asdf.git /asdf --branch v0.12.0

RUN . /asdf/asdf.sh && \
    asdf plugin add ruby && \
    asdf plugin add nodejs && \
    asdf nodejs update-nodebuild && \
    asdf plugin add postgres && \
    asdf plugin add python
